const path = require('path');

module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'dragiyski-serializer.bundle.js',
        libraryTarget: 'umd',
        globalObject: 'window'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                '@babel/preset-env',
                                {
                                    useBuiltIns: 'usage',
                                    corejs: 3,
                                    debug: true,
                                    exclude: [
                                        /^es\./,
                                        /^web\./,
                                        'proposal-object-rest-spread',
                                        'transform-async-to-generator',
                                        'transform-arrow-functions',
                                        'transform-block-scoped-functions',
                                        'transform-for-of',
                                        'transform-spread',
                                        'transform-typeof-symbol',
                                        'transform-new-target',
                                        'transform-regenerator',
                                        'transform-reserved-words',
                                        'proposal-async-generator-functions'
                                    ]
                                }
                            ]
                        ],
                        plugins: [
                            '@babel/plugin-proposal-private-methods'
                        ]
                    }
                }
            }
        ]
    },
    devtool: 'source-map'
};
