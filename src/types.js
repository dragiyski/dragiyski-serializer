import { NotSerializableError, UnserializeError } from './errors.js';

const flags = Symbol('flags');

const symbols = {
    environment: Symbol('environment'),
    class: Symbol('class')
};

export class Type {
    constructor(environment) {
        this[symbols.environment] = environment;
    }

    get id() {
        return this[symbols.environment].getTypeId(this);
    }

    get environment() {
        return this[symbols.environment];
    }

    serialize(value, context) {
        context[Type.id] = this.id;
    }

    sizeOf(context) {
        return this[symbols.environment].sizeOfVLQ(context[Type.id]);
    }

    write(context, target) {
        this[symbols.environment].writeVLQ(target, context[Type.id]);
    }

    read(source, context) {
        context[Type.id] = this[symbols.environment].readVLQ(source);
    }

    unserialize() {}
}

Object.defineProperties(Type, {
    id: {
        value: Symbol('id')
    }
});

export class HoleType extends Type {}

export class UndefinedType extends HoleType {
    read(source, context) {
        super.read(source, context);
        return undefined;
    }
}

export class NullType extends HoleType {
    read(source, context) {
        super.read(source, context);
        return null;
    }
}

export class BaseType extends Type {
    serialize(value, context) {
        super.serialize(value, context);
        context[flags] = 0;
    }

    sizeOf(context) {
        return super.sizeOf(context) + this.environment.sizeOfVLQ(context[flags]);
    }

    read(source, context) {
        super.read(source, context);
        context[flags] = this.environment.readVLQ(source);
    }

    write(context, target) {
        super.write(context, target);
        this.environment.writeVLQ(target, context[flags]);
    }
}

Object.defineProperties(Type, {
    flags: {
        value: flags
    }
});

export class BooleanType extends BaseType {
    serialize(value, context) {
        super.serialize(value, context);
        context[flags] |= value;
    }

    read(source, context) {
        super.read(source, context);
        return Boolean(context[flags] & 1);
    }
}

export class NumberType extends BaseType {
    serialize(value, context) {
        super.serialize(value, context);
        if (!isFinite(value)) {
            context[flags] |= 1;
            if (!isNaN(value)) {
                context[flags] |= 2;
                if (value < 0) {
                    context[flags] |= 4;
                }
            }
        } else {
            if (Object.is(value, -0) || value < 0) {
                context[flags] |= 4;
            }
            if (Number.isSafeInteger(value)) {
                context[flags] |= 2;
                context[NumberType.number] = context[flags] & 4 ? -value : value;
            } else {
                context[NumberType.number] = value;
            }
        }
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        if (!(context[flags] & 1)) {
            if (context[flags] & 2) {
                size += this.environment.sizeOfVLQ(context[NumberType.number]);
            } else {
                size += 8;
            }
        }
        return size;
    }

    write(context, target) {
        super.write(context, target);
        if (!(context[flags] & 1)) {
            if (context[flags] & 2) {
                this.environment.writeVLQ(target, context[NumberType.number]);
            } else {
                this.environment.writeFloat64(target, context[NumberType.number]);
            }
        }
    }

    read(source, context) {
        super.read(source, context);
        let value;
        if (context[flags] & 1) {
            if (context[flags] & 2) {
                if (context[flags] & 4) {
                    value = Number.NEGATIVE_INFINITY;
                } else {
                    value = Number.POSITIVE_INFINITY;
                }
            } else {
                value = Number.NaN;
            }
        } else if (context[flags] & 2) {
            value = this.environment.readVLQ(source);
            if (context[flags] & 4) {
                value = -value;
            }
        } else {
            value = this.environment.readFloat64(source);
        }
        return value;
    }
}

Object.defineProperties(NumberType, {
    number: {
        value: Symbol('number')
    }
});

export class BigIntType extends BaseType {
    serialize(value, context) {
        super.serialize(value, context);
        if (value < 0n) {
            context[flags] |= 4;
            value = -value;
        }
        const word64 = [];
        const word8 = [];
        while (value > 0xFFFFFFFFFFFFFFFFn) {
            word64.unshift(value & 0xFFFFFFFFFFFFFFFFn);
            value >>= 64n;
        }
        while (value > 0) {
            word8.unshift(Number(value & 0xFFn));
            value >>= 8n;
        }
        context[BigIntType.word64] = word64;
        context[BigIntType.word8] = word8;
        context[BigIntType.size] = word64.length * 8 + word8.length;
    }

    sizeOf(context) {
        let size = super.sizeOf(context) + context[BigIntType.size];
        size += this.environment.sizeOfVLQ(context[BigIntType.size]);
        return size;
    }

    write(context, target) {
        super.write(context, target);
        this.environment.writeVLQ(target, context[BigIntType.size]);
        for (const word of context[BigIntType.word8]) {
            this.environment.writeUint8(target, word);
        }
        for (const word of context[BigIntType.word64]) {
            this.environment.writeBigUint64(target, word);
        }
    }

    read(source, context) {
        super.read(source, context);
        let length = this.environment.readVLQ(source);
        let value = 0n;
        while (length >= 8) {
            value <<= 64n;
            value |= this.environment.readBigUInt64(source);
            length -= 8;
        }
        while (length > 0) {
            value <<= 8n;
            value |= BigInt(this.environment.readUint8(source));
            --length;
        }
        if (context[flags] & 4) {
            value = -value;
        }
        return value;
    }
}

Object.defineProperties(BigIntType, {
    number: {
        value: Symbol('number')
    },
    word64: {
        value: Symbol('word64')
    },
    word8: {
        value: Symbol('word8')
    },
    size: {
        value: Symbol('size')
    }
});

export class StringType extends BaseType {
    serialize(value, context) {
        super.serialize(value, context);
        const encoder = new TextEncoder();
        context[StringType.buffer] = encoder.encode(value);
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        const bufferSize = context[StringType.buffer].byteLength;
        size += this.environment.sizeOfVLQ(bufferSize);
        size += bufferSize;
        return size;
    }

    write(context, target) {
        super.write(context, target);
        const buffer = context[StringType.buffer];
        this.environment.writeVLQ(target, buffer.byteLength);
        target.uint8Array.set(buffer, target.offset);
        target.offset += buffer.byteLength;
    }

    read(source, context) {
        super.read(source, context);
        const length = this.environment.readVLQ(source);
        const view = new Uint8Array(
            source.dataView.buffer,
            source.dataView.byteOffset + source.offset,
            length
        );
        const decoder = new TextDecoder('utf-8', { fatal: true });
        return decoder.decode(view);
    }
}

Object.defineProperties(StringType, {
    buffer: {
        value: Symbol('buffer')
    }
});

export class ObjectType extends BaseType {
    constructor(environment, Class) {
        super(environment);
        this[symbols.class] = Class;
    }

    get class() {
        return this[symbols.class];
    }

    static serializeProperties(env, value, context) {
        const properties = context[ObjectType.properties] = [];
        let size = 0;
        for (const key of Object.keys(value)) {
            const keyRef = env.getContext(key).refIndex;
            const valueRef = env.getContext(value[key]).refIndex;
            const keyRefSize = env.sizeOfVLQ(keyRef);
            const valueRefSize = env.sizeOfVLQ(valueRef);
            properties.push({ keyRef, valueRef });
            size += keyRefSize + valueRefSize;
        }
        context[ObjectType.size] = size;
    }

    static writeProperties(env, context, target) {
        const properties = context[ObjectType.properties];
        env.writeVLQ(target, properties.length);
        for (const property of properties) {
            env.writeVLQ(target, property.keyRef);
            env.writeVLQ(target, property.valueRef);
        }
    }

    static readProperties(env, source, context) {
        const length = env.readVLQ(source);
        const properties = context[ObjectType.properties] = [];
        for (let i = 0; i < length; ++i) {
            const keyRef = env.readVLQ(source);
            const valueRef = env.readVLQ(source);
            properties.push({ keyRef, valueRef });
        }
    }

    static unserializeProperties(env, context, target) {
        const properties = context[ObjectType.properties];
        for (const property of properties) {
            target[env.getValue(property.keyRef)] = env.getValue(property.valueRef);
        }
    }

    static sizeOfProperties(env, context) {
        return context[ObjectType.size] + env.sizeOfVLQ(context[ObjectType.properties].length);
    }
}

Object.defineProperties(ObjectType, {
    properties: {
        value: Symbol('properties')
    },
    size: {
        value: Symbol('size')
    }
});

export class PlainObjectType extends ObjectType {
    serializePrototype(value, context) {
        if ('prototype' in context) {
            context[flags] |= 32;
        }
    }

    serialize(value, context) {
        super.serialize(value, context);
        this.serializePrototype(value, context);
        ObjectType.serializeProperties(this.environment, value, context);
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        if (context[flags] & 32) {
            size += this.environment.sizeOfVLQ(context.prototype);
        }
        size += ObjectType.sizeOfProperties(this.environment, context);
        return size;
    }

    write(context, target) {
        super.write(context, target);
        if (context[flags] & 32) {
            this.environment.writeVLQ(target, context.prototype);
        }
        ObjectType.writeProperties(this.environment, context, target);
    }

    readPrototype(source, context) {
        context.prototype = this.environment.readVLQ(source);
    }

    read(source, context) {
        super.read(source, context);
        if (context[flags] & 32) {
            this.readPrototype(source, context);
        }
        ObjectType.readProperties(this.environment, source, context);
        return {};
    }

    unserialize(context, value) {
        super.unserialize(context, value);
        if (context[flags] & 32) {
            const prototype = this.environment.getValue(context.prototype);
            Object.setPrototypeOf(value, prototype);
        }
        ObjectType.unserializeProperties(this.environment, context, value);
    }
}

export class ArrayType extends ObjectType {
    serialize(value, context) {
        super.serialize(value, context);
        const items = context[ArrayType.items] = [];
        let size = 0;
        let sequence = 0;
        value.forEach((element, index) => {
            if (sequence++ !== index) {
                context[flags] |= 1;
            }
            const valueRef = this.environment.getContext(element).refIndex;
            items.push({ index, valueRef });
            size += this.environment.sizeOfVLQ(valueRef);
        });
        context[ArrayType.length] = items.length;
        if (context[flags] & 1) {
            context[ArrayType.arrayLength] = value.length;
            for (const item of items) {
                item.indexRef = this.environment.getContext(item.index).refIndex;
                size += this.environment.sizeOfVLQ(item.indexRef);
            }
        }
        context[ArrayType.size] = size;
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        const length = context[ArrayType.length];
        size += this.environment.sizeOfVLQ(length);
        size += context[ArrayType.size];
        if (context[flags] & 1) {
            size += this.environment.sizeOfVLQ(context[ArrayType.arrayLength]);
        }
        return size;
    }

    write(context, target) {
        super.write(context, target);
        const length = context[ArrayType.length];
        this.environment.writeVLQ(target, length);
        if (context[flags] & 1) {
            this.environment.writeVLQ(target, context[ArrayType.arrayLength]);
            for (const item of context[ArrayType.items]) {
                this.environment.writeVLQ(target, item.indexRef);
                this.environment.writeVLQ(target, item.valueRef);
            }
        } else {
            for (const item of context[ArrayType.items]) {
                this.environment.writeVLQ(target, item.valueRef);
            }
        }
    }

    read(source, context) {
        super.read(source, context);
        const length = context[ArrayType.length] = this.environment.readVLQ(source);
        const items = context[ArrayType.items] = [];
        if (context[flags] & 1) {
            context[ArrayType.arrayLength] = this.environment.readVLQ(source);
            for (let i = 0; i < length; ++i) {
                const indexRef = this.environment.readVLQ(source);
                const valueRef = this.environment.readVLQ(source);
                items.push({ indexRef, valueRef });
            }
            return new Array(context[ArrayType.arrayLength]);
        }
        for (let i = 0; i < length; ++i) {
            const valueRef = this.environment.readVLQ(source);
            items.push({ valueRef });
        }
        return [];
    }

    unserialize(context, value) {
        super.unserialize(context, value);
        const items = context[ArrayType.items];
        if (context[flags] & 1) {
            for (const item of items) {
                value[this.environment.getValue(item.indexRef)] = this.environment.getValue(item.valueRef);
            }
        } else {
            for (const item of items) {
                value.push(this.environment.getValue(item.valueRef));
            }
        }
    }
}

Object.defineProperties(ArrayType, {
    items: {
        value: Symbol('items')
    },
    size: {
        value: Symbol('size')
    },
    length: {
        value: Symbol('length')
    },
    arrayLength: {
        value: Symbol('arrayLength')
    }
});

export class DateType extends ObjectType {
    serialize(value, context) {
        super.serialize(value, context);
        context[DateType.timestamp] = value.valueOf();
    }

    sizeOf(context) {
        return super.sizeOf(context) + this.environment.sizeOfVLQ(context[DateType.timestamp]);
    }

    write(context, target) {
        super.write(context, target);
        this.environment.writeVLQ(target, context[DateType.timestamp]);
    }

    read(source, context) {
        super.read(source, context);
        const timestamp = this.environment.readVLQ(source);
        return new Date(timestamp);
    }
}

Object.defineProperties(DateType, {
    timestamp: {
        value: Symbol('timestamp')
    }
});

export class RegExpType extends ObjectType {
    serialize(value, context) {
        super.serialize(value, context);
        const flags = value.flags.split('').sort().map(c => c.codePointAt(0));
        if (flags.length > 127) {
            throw new NotSerializableError('Unexpected RegExp object with more than 127 flags');
        }
        if (!flags.every(flag => flag >= 32 && flag <= 126)) {
            throw new NotSerializableError('Unexpected RegExp object with non-ascii flags');
        }
        context[RegExpType.flags] = flags;
        context[RegExpType.source] = this.environment.getContext(value.source, false).refIndex;
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        const flagLength = context[RegExpType.flags].length;
        size += flagLength;
        size += this.environment.sizeOfVLQ(flagLength);
        size += this.environment.sizeOfVLQ(context[RegExpType.source]);
        return size;
    }

    write(context, target) {
        super.write(context, target);
        const flags = context[RegExpType.flags];
        const length = flags.length;
        this.environment.writeVLQ(target, length);
        for (let i = 0; i < length; ++i) {
            this.environment.writeUint8(target, flags[i]);
        }
        this.environment.writeVLQ(target, context[RegExpType.source]);
    }

    read(source, context) {
        super.read(source, context);
        const length = this.environment.readVLQ(source);
        if (length > 127) {
            throw new UnserializeError('Unexpected RegExp flags length > 127');
        }
        let flags = [];
        for (let i = 0; i < length; ++i) {
            const byte = this.environment.readUint8(source);
            if (byte < 32 || byte > 126) {
                throw new UnserializeError(`RegExp flag not ASCII: ${byte}`);
            }
            flags.push(String.fromCodePoint(byte));
        }
        flags = flags.join('');
        context[RegExpType.flags] = flags;
        const sourceRef = this.environment.readVLQ(source);
        const sourceValue = this.environment.getValue(sourceRef);
        if (typeof sourceValue !== 'string') {
            throw new UnserializeError(`Expected the source of RegExp object to be a string, got ${typeof sourceValue}`);
        }
        return new RegExp(sourceValue, flags);
    }
}

Object.defineProperties(RegExpType, {
    flags: {
        value: Symbol('flag')
    },
    source: {
        value: Symbol('source')
    }
});

export class ArrayBufferType extends ObjectType {
    serialize(value, context) {
        super.serialize(value, context);
        context[ArrayBufferType.buffer] = value;
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        const byteLength = context[ArrayType.buffer].byteLength;
        size += this.environment.sizeOfVLQ(byteLength);
        size += byteLength;
        return size;
    }

    write(context, target) {
        super.write(context, target);
        const buffer = context[ArrayType.buffer];
        this.environment.writeVLQ(target, buffer.byteLength);
        const sourceView = new Uint8Array(buffer);
        target.uint8Array.set(sourceView, target.offset);
        target.offset += buffer.byteLength;
    }

    read(source, context) {
        super.read(source, context);
        const byteLength = this.environment.readVLQ(source);
        const buffer = source.dataView.buffer.slice(
            source.dataView.byteOffset + source.offset,
            source.dataView.byteOffset + source.offset + byteLength
        );
        source.offset += byteLength;
        return buffer;
    }
}

Object.defineProperties(ArrayBufferType, {
    buffer: {
        value: Symbol('buffer')
    }
});

export class TypedArrayType extends ObjectType {
    serialize(value, context) {
        super.serialize(value, context);
        context[TypedArrayType.buffer] = value;
    }

    sizeOf(context) {
        let size = super.sizeOf(context);
        const byteLength = context[TypedArrayType.buffer].byteLength;
        size += this.environment.sizeOfVLQ(byteLength);
        size += byteLength;
        return size;
    }

    write(context, target) {
        super.write(context, target);
        const buffer = context[TypedArrayType.buffer];
        this.environment.writeVLQ(target, buffer.byteLength);
        const sourceView = new Uint8Array(buffer.buffer, buffer.byteOffset, buffer.byteLength);
        target.uint8Array.set(sourceView, target.offset);
        target.offset += sourceView.byteLength;
    }

    read(source, context) {
        super.read(source, context);
        const byteLength = this.environment.readVLQ(source);
        const buffer = source.dataView.buffer.slice(
            source.dataView.byteOffset + source.offset,
            source.dataView.byteOffset + source.offset + byteLength
        );
        // eslint-disable-next-line new-cap
        return new this.class(buffer);
    }
}

Object.defineProperties(TypedArrayType, {
    buffer: {
        value: Symbol('buffer')
    }
});

export class SetType extends ObjectType {
    serialize(value, context) {
        super.serialize(value, context);
        const items = [];
        let size = 0;
        for (const item of value) {
            const itemRef = this.environment.getContext(item).refIndex;
            items.push(itemRef);
            size += this.environment.sizeOfVLQ(itemRef);
        }
        context[SetType.size] = size;
        context[SetType.items] = items;
    }

    sizeOf(context) {
        return super.sizeOf(context) + this.environment.sizeOfVLQ(context[SetType.items].length) + context[SetType.size];
    }

    write(context, target) {
        super.write(context, target);
        const items = context[SetType.items];
        this.environment.writeVLQ(target, items.length);
        for (const item of items) {
            this.environment.writeVLQ(target, item);
        }
    }

    read(source, context) {
        super.read(source, context);
        const length = this.environment.readVLQ(source);
        const items = [];
        for (let i = 0; i < length; ++i) {
            items.push(this.environment.readVLQ(source));
        }
        context[SetType.items] = items;
        return new Set();
    }

    unserialize(context, set) {
        super.unserialize(context, set);
        const items = context[SetType.items];
        for (const item of items) {
            const value = this.environment.getValue(item);
            if (set.has(value)) {
                throw new UnserializeError(`Unable to unserialize a Set with duplicates`);
            }
            set.add(value);
        }
    }
}

Object.defineProperties(SetType, {
    size: {
        value: Symbol('size')
    },
    items: {
        value: Symbol('items')
    }
});

export class MapType extends ObjectType {
    serialize(map, context) {
        super.serialize(map, context);
        const items = [];
        let size = 0;
        for (const [key, value] of map) {
            const keyRef = this.environment.getContext(key).refIndex;
            const valueRef = this.environment.getContext(value).refIndex;
            items.push({ keyRef, valueRef });
            size += this.environment.sizeOfVLQ(keyRef);
            size += this.environment.sizeOfVLQ(valueRef);
        }
        context[MapType.size] = size;
        context[MapType.items] = items;
    }

    sizeOf(context) {
        return super.sizeOf(context) + this.environment.sizeOfVLQ(context[MapType.items].length) + context[MapType.size];
    }

    write(context, target) {
        super.write(context, target);
        const items = context[MapType.items];
        this.environment.writeVLQ(target, items.length);
        for (const item of items) {
            this.environment.writeVLQ(target, item.keyRef);
            this.environment.writeVLQ(target, item.valueRef);
        }
    }

    read(source, context) {
        super.read(source, context);
        const length = this.environment.readVLQ(source);
        const items = [];
        for (let i = 0; i < length; ++i) {
            const keyRef = this.environment.readVLQ(source);
            const valueRef = this.environment.readVLQ(source);
            items.push({ keyRef, valueRef });
        }
        context[MapType.items] = items;
        return new Map();
    }

    unserialize(context, map) {
        super.unserialize(context, map);
        const items = context[MapType.items];
        for (const item of items) {
            const key = this.environment.getValue(item.keyRef);
            const value = this.environment.getValue(item.valueRef);
            if (map.has(key)) {
                throw new UnserializeError(`Unable to unserialize a Map with duplicates`);
            }
            map.set(key, value);
        }
    }
}

Object.defineProperties(MapType, {
    size: {
        value: Symbol('size')
    },
    items: {
        value: Symbol('items')
    }
});
