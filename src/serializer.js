import * as types from './types.js';
import { NotSerializableError, UnserializeError } from './errors.js';

const hasOwnProperty = Object.prototype.hasOwnProperty;

const symbols = {
    typeList: Symbol('typeList'),
    typeClassMap: Symbol('typeClassMap'),
    serialize: Symbol('serialize'),
    unserialize: Symbol('unserialize'),
    convertMap: Symbol('convertMap'),
    valueMap: Symbol('valueMap'),
    refList: Symbol('refList'),
    serializer: Symbol('serializer'),
    value: Symbol('value'),
    source: Symbol('source'),
    valueResolveStack: Symbol('valueResolveStack')
};

const methods = {
    buildTypeStructures: Symbol('buildTypeStructures')
};

export default class Serializer {
    constructor(options) {
        this[symbols.typeList] = [];
        this[symbols.typeClassMap] = new Map();
        this[symbols.serialize] = identity;
        this[symbols.unserialize] = identity;
        options = { ...options };
        this[symbols.typeList].push('undefined');
        this[symbols.typeList].push('null');
        this[symbols.typeList].push(Boolean);
        this[symbols.typeList].push(Number);
        this[symbols.typeList].push(BigInt);
        this[symbols.typeList].push(String);
        this[symbols.typeList].push(Object);
        this[symbols.typeList].push(Array);
        this[symbols.typeList].push(Date);
        this[symbols.typeList].push(RegExp);
        this[symbols.typeList].push(ArrayBuffer);
        this[symbols.typeList].push(Int8Array);
        this[symbols.typeList].push(Int16Array);
        this[symbols.typeList].push(Int32Array);
        this[symbols.typeList].push(BigInt64Array);
        this[symbols.typeList].push(Uint8Array);
        this[symbols.typeList].push(Uint8ClampedArray);
        this[symbols.typeList].push(Uint16Array);
        this[symbols.typeList].push(Uint32Array);
        this[symbols.typeList].push(BigUint64Array);
        this[symbols.typeList].push(Float32Array);
        this[symbols.typeList].push(Float64Array);
        this[symbols.typeList].push(DataView);
        this[symbols.typeList].push(Set);
        this[symbols.typeList].push(Map);
        this[symbols.typeClassMap].set('undefined', types.UndefinedType);
        this[symbols.typeClassMap].set('null', types.NullType);
        this[symbols.typeClassMap].set('boolean', types.BooleanType);
        this[symbols.typeClassMap].set('number', types.NumberType);
        this[symbols.typeClassMap].set('bigint', types.BigIntType);
        this[symbols.typeClassMap].set('string', types.StringType);
        this[symbols.typeClassMap].set(Boolean, types.BooleanType);
        this[symbols.typeClassMap].set(Number, types.NumberType);
        this[symbols.typeClassMap].set(BigInt, types.BigIntType);
        this[symbols.typeClassMap].set(String, types.StringType);
        this[symbols.typeClassMap].set(Object, types.PlainObjectType);
        this[symbols.typeClassMap].set(Array, types.ArrayType);
        this[symbols.typeClassMap].set(Date, types.DateType);
        this[symbols.typeClassMap].set(RegExp, types.RegExpType);
        this[symbols.typeClassMap].set(ArrayBuffer, types.ArrayBufferType);
        this[symbols.typeClassMap].set(Int8Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Int16Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Int32Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(BigInt64Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Uint8Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Uint8ClampedArray, types.TypedArrayType);
        this[symbols.typeClassMap].set(Uint16Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Uint32Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(BigUint64Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Float32Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(Float64Array, types.TypedArrayType);
        this[symbols.typeClassMap].set(DataView, types.TypedArrayType);
        this[symbols.typeClassMap].set(Set, types.SetType);
        this[symbols.typeClassMap].set(Map, types.MapType);
        if (typeof options.serialize === 'function') {
            this[symbols.serialize] = options.serialize;
        }
        if (typeof options.unserialize === 'function') {
            this[symbols.unserialize] = options.unserialize;
        }
    }

    [methods.buildTypeStructures](environment) {
        environment.typeMap = new Map();
        environment.typeList = [];
        for (let id = 0; id < this[symbols.typeList].length; ++id) {
            const Class = this[symbols.typeList][id];
            const TypeClass = this[symbols.typeClassMap].get(Class);
            const type = typeof Class === 'function' ? new TypeClass(environment, Class) : new TypeClass(environment);
            environment.typeMap.set(Class, type);
            environment.typeList[id] = type;
        }
        environment.typeMap.set('boolean', environment.typeMap.get(Boolean));
        environment.typeMap.set('number', environment.typeMap.get(Number));
        environment.typeMap.set('bigint', environment.typeMap.get(BigInt));
        environment.typeMap.set('string', environment.typeMap.get(String));
    }

    serialize(value) {
        const environment = new SerializeEnvironment(this, value);
        this[methods.buildTypeStructures](environment);
        environment.convert = this[symbols.serialize];
        return environment.execute();
    }

    unserialize(buffer) {
        const environment = new UnserializeEnvironment(this, buffer);
        this[methods.buildTypeStructures](environment);
        environment.convert = this[symbols.unserialize];
        return environment.execute();
    }
}

Object.defineProperties(Serializer, {
    tag: {
        value: Symbol('tag')
    },
    properties: {
        value: Symbol('properties')
    }
});

class Environment {
    getTypeId(type) {
        return this.typeList.indexOf(type);
    }

    readVLQ(source) {
        let number = 0n;
        let length = 0;
        let lastByte;
        for (let i = 0; i < 8; ++i) {
            lastByte = source.dataView.getUint8(source.offset + length++);
            number <<= 7n;
            number |= BigInt(lastByte & 0x7F);
            if (!(lastByte & 0x80)) {
                break;
            }
        }
        if (lastByte & 0x80) {
            throw new UnserializeError(`Invalid data: maximum VLQ size is 8 bytes`);
        }
        if (number > BigInt(Number.MAX_SAFE_INTEGER)) {
            throw new UnserializeError(`Invalid data: VLQ decoded into number larger than Number.MAX_SAFE_INTEGER`);
        }
        source.offset += length;
        return Number(number);
    }

    sizeOfVLQ(number) {
        let length = 0;
        number = BigInt(number);
        while (number > 0) {
            length++;
            number >>= 7n;
        }
        if (length <= 0) {
            return 1;
        }
        return length;
    }

    writeVLQ(target, number) {
        number = BigInt(number);
        const bytes = [];
        while (number > 0) {
            bytes.unshift(number & 0x7Fn);
            number >>= 7n;
        }
        if (bytes.length <= 0) {
            bytes.push(0);
        }0
        for (let i = 0, length = bytes.length - 1; i < length; ++i) {
            target.dataView.setUint8(target.offset++, Number(bytes[i] | 0x80n));
        }
        target.dataView.setUint8(target.offset++, Number(bytes[bytes.length - 1]));
    }

    readUint8(source) {
        return source.dataView.getUint8(source.offset++);
    }

    writeUint8(target, value) {
        target.dataView.setUint8(target.offset++, value);
    }

    readBigUInt64(source) {
        const value = source.dataView.getBigUint64(source.offset);
        source.offset += 8;
        return value;
    }

    writeBigUint64(target, value) {
        target.dataView.setBigUint64(target.offset, value);
        target.offset += 8;
    }

    readFloat64(source) {
        const value = source.dataView.getFloat64(source.offset);
        source.offset += 8;
        return value;
    }

    writeFloat64(target, value) {
        target.dataView.setFloat64(target.offset, value);
        target.offset += 8;
    }
}

class SerializeEnvironment extends Environment {
    constructor(serializer, value) {
        super();
        this[symbols.convertMap] = new Map();
        this[symbols.valueMap] = new Map();
        this[symbols.refList] = [];
        this[symbols.serializer] = serializer;
        this[symbols.value] = value;
    }

    initializeValueType(value, context, allowObject = true) {
        if (value === undefined) {
            Object.defineProperty(context, 'type', {
                value: this.typeMap.get('undefined')
            });
        } else if (value === null) {
            Object.defineProperty(context, 'type', {
                value: this.typeMap.get('null')
            });
        } else {
            const typeOfValue = typeof value;
            if (typeOfValue === 'object') {
                if (!allowObject) {
                    throw new NotSerializableError(`Serialization of object, on a location where object is not expected`);
                }
                const prototype = Object.getPrototypeOf(value);
                if (prototype == null) {
                    // Object with NULL prototype are serializable
                    Object.defineProperty(context, 'type', {
                        value: this.typeMap.get(Object)
                    });
                    context.prototype = this.getContext(null).refIndex;
                } else if (
                    !hasOwnProperty.call(prototype, 'constructor') ||
                    !hasOwnProperty.call(Object.getOwnPropertyDescriptor(prototype, 'constructor'), 'value') ||
                    typeof prototype.constructor !== 'function'
                ) {
                    // Non-constructed objects are also serializable (i.e. Object.create(<object>))
                    Object.defineProperty(context, 'type', {
                        value: this.typeMap.get(Object)
                    });
                    context.prototype = this.getContext(prototype).refIndex;
                } else if (this.typeMap.has(prototype.constructor)) {
                    Object.defineProperty(context, 'type', {
                        value: this.typeMap.get(prototype.constructor)
                    });
                } else {
                    for (const [Class, type] of this.typeMap) {
                        if (typeof Class !== 'function') {
                            continue;
                        }
                        if (Class !== Object && value instanceof Class) {
                            Object.defineProperty(context, 'type', {
                                value: type
                            });
                            break;
                        }
                    }
                    if (context.type == null) {
                        throw new NotSerializableError(`Objects of type '${prototype.constructor.name}' are not serializable`);
                    }
                }
            } else {
                if (!this.typeMap.has(typeOfValue)) {
                    throw new NotSerializableError(`Values of type '${typeOfValue}' are not serializable`);
                }
                Object.defineProperty(context, 'type', {
                    value: this.typeMap.get(typeOfValue)
                });
            }
        }
    }

    getContext(value, allowObject = true) {
        while (true) {
            if (valueMapContains(this[symbols.valueMap], value)) {
                return getFromValueMap(this[symbols.valueMap], value);
            }
            if (value != null && typeof value === 'object') {
                if (value instanceof Boolean || value instanceof Number || value instanceof String || value instanceof BigInt) {
                    value = value.valueOf();
                }
                if (valueMapContains(this[symbols.convertMap], value)) {
                    value = this[symbols.convertMap].get(value);
                    continue;
                }
                const newValue = this.convert(value);
                if (newValue !== value) {
                    saveIntoValueMap(this[symbols.convertMap], value, newValue);
                    value = newValue;
                    continue;
                }
            }
            break;
        }
        const context = Object.create(null, {
            refIndex: {
                value: allocateIndex(this[symbols.refList], value)
            }
        });
        saveIntoValueMap(this[symbols.valueMap], value, context);
        this.initializeValueType(value, context, allowObject);
        context.type.serialize(value, context);
        return context;
    }

    execute() {
        // Step 1: Store all references (this also create the context objects)
        const mainRef = this.getContext(this[symbols.value]).refIndex;
        // Step 2: Evaluate the buffer required to store all the data
        let size = 0;
        let lastReferenceOffset = 0;
        const references = [];
        for (const value of this[symbols.refList]) {
            const context = getFromValueMap(this[symbols.valueMap], value);
            const length = context.type.sizeOf(context);
            size += length;
            size += this.sizeOfVLQ(length);
            references.push({
                offset: lastReferenceOffset,
                length,
                value
            });
            lastReferenceOffset += length;
        }
        size += this.sizeOfVLQ(references.length);
        size += this.sizeOfVLQ(mainRef);
        // Step 3: Create the buffer
        const target = {
            buffer: new ArrayBuffer(size),
            offset: 0
        };
        target.dataView = new DataView(target.buffer);
        target.uint8Array = new Uint8Array(target.buffer);
        // Step 4: Write the header
        this.writeVLQ(target, mainRef);
        this.writeVLQ(target, references.length);
        for (const reference of references) {
            this.writeVLQ(target, reference.length);
        }
        // Step 5: Write the main data
        for (const reference of references) {
            const context = getFromValueMap(this[symbols.valueMap], reference.value);
            const valueTarget = {
                buffer: target.buffer,
                dataView: new DataView(target.buffer, target.offset + reference.offset, reference.length),
                uint8Array: new Uint8Array(target.buffer, target.offset + reference.offset, reference.length),
                offset: 0
            };
            context.type.write(context, valueTarget);
        }
        return target.buffer;
    }
}

class UnserializeEnvironment extends Environment {
    constructor(serializer, buffer) {
        super();
        this[symbols.convertMap] = new Map();
        this[symbols.refList] = [];
        this[symbols.valueResolveStack] = [];
        this[symbols.serializer] = serializer;
        this[symbols.source] = {
            offset: 0
        };
        if (ArrayBuffer.isView(buffer)) {
            this[symbols.source].dataView = new DataView(buffer.buffer, buffer.byteOffset, buffer.byteLength);
            this[symbols.source].uint8Array = new Uint8Array(buffer.buffer, buffer.byteOffset, buffer.byteLength);
        } else if (buffer instanceof ArrayBuffer) {
            this[symbols.source].dataView = new DataView(buffer);
            this[symbols.source].uint8Array = new Uint8Array(buffer);
        } else {
            throw new TypeError('Not a buffer');
        }
    }

    getValue(index) {
        const reference = this[symbols.refList][index];
        if ('value' in reference) {
            return reference.value;
        }
        if (this[symbols.valueResolveStack].indexOf(index) >= 0) {
            throw new UnserializeError(`Early resolving of values resulted in circular reference: ${this[symbols.valueResolveStack].join(', ')} + ', ' + ${index}`);
        }
        this[symbols.valueResolveStack].push(index);
        const valueSource = {
            dataView: new DataView(this[symbols.source].uint8Array.buffer, this[symbols.source].uint8Array.byteOffset + this[symbols.source].offset + reference.offset, reference.length),
            uint8Array: new Uint8Array(this[symbols.source].uint8Array.buffer, this[symbols.source].uint8Array.byteOffset + this[symbols.source].offset + reference.offset, reference.length),
            offset: 0
        };
        const typeId = this.readVLQ(valueSource);
        valueSource.offset = 0;
        if (typeId > this.typeList.length || !(typeId in this.typeList)) {
            throw new UnserializeError(`Reference to unknown type with ID: ${typeId}`);
        }
        const type = this.typeList[typeId];
        Object.defineProperty(reference.context, 'type', {
            value: type
        });
        let value = type.read(valueSource, reference.context);
        while (true) {
            if (value != null && typeof value === 'object') {
                if (value instanceof Boolean || value instanceof Number || value instanceof String || value instanceof BigInt) {
                    value = value.valueOf();
                    break;
                }
                if (valueMapContains(this[symbols.convertMap], value)) {
                    value = this[symbols.convertMap].get(value);
                    continue;
                }
                const newValue = this.convert(value);
                if (newValue !== value) {
                    saveIntoValueMap(this[symbols.convertMap], value, newValue);
                    value = newValue;
                    continue;
                }
            }
            break;
        }
        reference.value = value;
        this[symbols.valueResolveStack].pop();
        return reference.value;
    }

    execute() {
        const source = this[symbols.source];
        const mainRef = this.readVLQ(source);
        const refListLength = this.readVLQ(source);
        if (mainRef >= refListLength) {
            throw new UnserializeError(`The main reference is outside boundaries of the reference list`);
        }
        let lastReferenceOffset = 0;
        for (let i = 0; i < refListLength; ++i) {
            const length = this.readVLQ(source);
            this[symbols.refList].push({
                offset: lastReferenceOffset,
                length,
                context: Object.create(null)
            });
            lastReferenceOffset += length;
        }
        if (lastReferenceOffset + source.offset > source.uint8Array.byteLength) {
            throw new UnserializeError(`Serialized buffer too small, expected minumum ${lastReferenceOffset + source.offset} bytes, got ${source.uint8Array.byteLength} bytes`);
        }
        for (let i = 0; i < this[symbols.refList].length; ++i) {
            const reference = this[symbols.refList][i];
            reference.value = this.getValue(i);
        }
        for (let i = 0; i < this[symbols.refList].length; ++i) {
            const reference = this[symbols.refList][i];
            reference.context.type.unserialize(reference.context, reference.value);
        }
        return this[symbols.refList][mainRef].value;
    }
}

function allocateIndex(list, value) {
    const refIndex = list.length;
    list.push(value);
    return refIndex;
}

function getFromValueMap(map, key) {
    if (Object.is(key, -0)) {
        return map.negativeZero;
    }
    return map.get(key);
}

function saveIntoValueMap(map, key, value) {
    if (Object.is(key, -0)) {
        map.negativeZero = value;
    }
    map.set(key, value);
}

function valueMapContains(map, key) {
    if (Object.is(key, -0)) {
        return map.negativeZero != null;
    }
    return map.has(key);
}

function identity(value) {
    return value;
}
