export class NotSerializableError extends Error {}

export class UnserializeError extends Error {}
